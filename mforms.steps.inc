<?php

/**
 * @file
 * Single class that is a helper to deal with steps.
 */

/**
 * Object that deals with form steps.
 */
class MformsSteps {

  private $store;
  private static $instances = array();

  /**
   * Singleton.
   *
   * @param MformsIstore $store
   * @return MformsSteps
   * @throws Exception
   */
  public static function getInstance(MformsIstore $store) {
    if (empty($store)) {
      throw new Exception('Store not provided');
    }

    if (empty(self::$instances[$store->getKey()])) {
      self::$instances[$store->getKey()] = new MformsSteps($store);
    }

    return self::$instances[$store->getKey()];
  }

  private function __construct(MformsIstore $store) {
    $this->store = $store;
  }

  /**
   * Gets all steps.
   *
   * @return array
   */
  function getSteps() {
    $steps = $this->store->getStore('steps');
    if (is_array($steps)) {
      return $steps;
    }
    return array();
  }

  /**
   * Gets current step.
   *
   * @return string
   */
  function getCurr() {
    return $this->store->getStore('curr_step');
  }

  /**
   * Gets next step.
   *
   * @return string
   */
  function getNext() {
    $steps = $this->getSteps();
    $i = array_search($this->getCurr(), $steps);

    if (isset($steps[$i + 1])) {
      return $steps[$i + 1];
    }

    return NULL;
  }

  /**
   * Overrides next step in case it exists.
   *
   * @param $step
   */
  function overrideNext($step) {
    $steps = $this->getSteps();
    $i = array_search($this->getCurr(), $steps);

    if (isset($steps[$i + 1])) {
      $steps[$i + 1] = $step;
      $this->setSteps($steps);
    }
  }

  /**
   * Gets previous step.
   *
   * @return string
   */
  function getPrev() {
    $steps = $this->getSteps();
    $i = array_search($this->getCurr(), $steps);

    if (isset($steps[$i - 1])) {
      return $steps[$i - 1];
    }

    return NULL;
  }

  /**
   * Sets current step.
   *
   * @param string $step
   */
  function setCurr($step) {
    $this->store->setStore('curr_step', $step);
  }

  /**
   * Adds step to the steps chain.
   *
   * @param string $step
   */
  function addStep($step) {
    if (empty($step)) {
      return;
    }

    $steps = $this->getSteps();
    if (empty($steps) || !in_array($step, $steps)) {
      $steps[] = $step;
      $this->setSteps($steps);
    }
  }

  /**
   * Sets steps.
   *
   * @param array $steps
   */
  private function setSteps($steps) {
    $this->store->setStore('steps', $steps);
  }
}
