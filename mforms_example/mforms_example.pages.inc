<?php

/**
 * @file
 *   Mforms examples.
 */

/**
 * Entry page for the multi-step form.
 *
 * @return string - HTML form output
 */
function mforms_example_fs_store_page() {

  $description = '<div class="messages status">' . t('Demonstrates multistep form which uses Drupal\'s $form_state array as storage. If you refresh the page all filled in data get lost. Same happends if you load the page again. Such scenario is fine for two step forms (i.e. enter data than confirm) or for forms where users do not expect the data they filled in are persisted for longer time.') . '</div>';

  // Define constat for form_state store implementation
  define('FORM_STEPS_ID', 'fs_store_example');

  // Load desired store implementation.
  $store = MformsFormStateStore::getInstance(FORM_STEPS_ID);
  // Load steps object.
  $steps = MformsSteps::getInstance($store);
  // Load desired controls implementation.
  $controls = MformsSimpleControls::getInstance($store, $steps);

  // Just an example how to pass additional parameters into mfors
  $params = array('param1', 'objectN');

  // Init the mforms module so that mforms will be able to search for form pages
  mforms_init_module('mforms_example', FORM_STEPS_ID, $store, $controls);

  // Call drupal function to render form
  return $description . drupal_render(drupal_get_form('mforms_example_form', $params));
}

/**
 * Entry page for the multi-step form.
 *
 * @return string - HTML form output
 */
function mforms_example_session_store_page() {

  $description = '<div class="messages status">' . t('Demonstrates multistep form which uses $_SESSION object as storage. If you refresh the page all filled in data will remain in fields. Same happends if you load the page again. Moreover, the last step you were at is displayed. This approach is suitable for complex forms, where it is important to provide users with more time to fill in all data and prevent data loss in case of page reload or accidental window close, etc.') . '</div>';

  // Define constat for session store implementation
  define('FORM_STEPS_ID', 'session_store_example');

  $store = MformsSessionStore::getInstance(FORM_STEPS_ID);
  $steps = MformsSteps::getInstance($store);
  $controls = MformsMultiStepControls::getInstance($store, $steps, array(
    '_mforms_example_step1' => array('value' => 'step 1', 'weight' => -103),
    '_mforms_example_step2' => array('value' => 'step 2', 'weight' => -102),
    '_mforms_example_step3' => array('value' => 'step 3', 'weight' => -101),
    '_mforms_example_step4' => array('value' => 'step 4', 'weight' => -100),
  ));

  // Just an example how to pass additional parameters into mfors
  $params = array('param1', 'objectN');

  // Init the mforms module so that mforms will be able to search for form pages
  mforms_init_module('mforms_example', FORM_STEPS_ID, $store, $controls);

  // Call drupal function to render form
  return $description . drupal_render(drupal_get_form('mforms_example_form', $params));
}

/**
 * Callback function to generate form.
 *
 * @param array $form
 * @param array $form_state
 * @param array $params
 * @return array - form definition
 */
function mforms_example_form($form, &$form_state, $params) {

  // Call mforms_build(); function which is a wrapper around the Mform state machine. We pass in following parameters:
  //  - store key under which all data is stored
  //  - first step, which is a callback that returns the form array for the first screen of the form
  //  - $form_state - regular Drupal's form state array
  //  - $params - additional parameters that we need inside the form building callbacks
  return mforms_build(FORM_STEPS_ID, '_mforms_example_step1', $form_state);
}

/**
 * Callback function to validate form inputs
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_form_validate($form, &$form_state) {

  // Call mforms_validate(); function that runs the validate callback for current step. Params are as follows:
  //  - store key
  //  - $form - Drupal's form array
  //  - $form_state - Drupal's form state array
  mforms_validate(FORM_STEPS_ID, $form, $form_state);
}

/**
 * Callback function to process the form inputs.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_form_submit($form, &$form_state) {
  // Call mforms_submit(); function that runs the submit callback for current step.
  // Params are the same as for validate action.
  mforms_submit(FORM_STEPS_ID, $form, $form_state);
}

/**
 * This is a callback to display several forms on one page.
 *
 * @return string
 */
function mforms_example_more_forms_page() {
  global $user;

  $store1 = MformsFormStateStore::getInstance('more_forms_1');
  $steps1 = MformsSteps::getInstance($store1);
  $controls1 = MformsPPControls::getInstance($store1, $steps1);
  mforms_init_module('mforms_example', 'more_forms_1', $store1, $controls1);

  $store2 = MformsFormStateStore::getInstance('more_forms_2');
  $steps2 = MformsSteps::getInstance($store2);
  $controls2 = MformsPPControls::getInstance($store2, $steps2);
  mforms_init_module('mforms_example', 'more_forms_2', $store2, $controls2);

  $output = '';
  $output .= drupal_render(drupal_get_form('mforms_example_more_forms_1_form', $user));
  $output .= drupal_render(drupal_get_form('mforms_example_more_forms_2_form', $user));

  return '<div class="messages status">' . t('This example demonstrates how several forms can live on one page.') . '</div>' . $output;
}

/**
 * Form build callback.
 *
 * @param array $form
 * @param array $form_state
 * @param mixed $params
 * @return array
 */
function mforms_example_more_forms_1_form($form, &$form_state, $params) {
  return mforms_build('more_forms_1', '_mforms_example_more_forms_1_s1', $form_state, $params);
}

/**
 * Form validate callback.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_more_forms_1_form_validate($form, &$form_state) {
  mforms_validate('more_forms_1', $form, $form_state);
}

/**
 * Form submit callback.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_more_forms_1_form_submit($form, &$form_state) {
  mforms_submit('more_forms_1', $form, $form_state);
}

/**
 * Form build callback.
 *
 * @param array $form
 * @param array $form_state
 * @param mixed $params
 * @return array
 */
function mforms_example_more_forms_2_form($form, &$form_state, $params) {
  return mforms_build('more_forms_2', '_mforms_example_more_forms_2_s1', $form_state, $params);
}

/**
 * Form validate callback.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_more_forms_2_form_validate($form, &$form_state) {
  mforms_validate('more_forms_2', $form, $form_state);
}

/**
 * Form submit callback.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_more_forms_2_form_submit($form, &$form_state) {
  mforms_submit('more_forms_2', $form, $form_state);
}

/**
 * Page callback for multistep form with ajax example.
 *
 * @return array
 */
function mforms_example_ajax_forms_page() {
  $store = MformsFormStateStore::getInstance('ajax_forms');
  $steps = MformsSteps::getInstance($store);
  $controls = MformsSimpleControls::getInstance($store, $steps);

  mforms_init_module('mforms_example', 'ajax_forms', $store, $controls);

  return drupal_get_form('mforms_example_ajax_form');
}

/**
 * Inits mforms in ajax calls. Called as #after_build callback on steps that do
 * ajax actions.
 *
 * @param array $form
 * @return array
 */
function _mforms_example_init_mforms($form) {
  $store = MformsFormStateStore::getInstance('ajax_forms');
  $steps = MformsSteps::getInstance($store);
  $controls = MformsSimpleControls::getInstance($store, $steps);

  mforms_init_module('mforms_example', 'ajax_forms', $store, $controls);

  return $form;
}

/**
 * Form build callback.
 *
 * @param array $form
 * @param array $form_state
 * @return array
 */
function mforms_example_ajax_form($form, &$form_state) {
  return mforms_build('ajax_forms', '_mforms_example_ajax_forms_s1', $form_state);
}

/**
 * Form validate callback.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_ajax_form_validate($form, &$form_state) {
  mforms_validate('ajax_forms', $form, $form_state);
}

/**
 * Form submit callback.
 *
 * @param array $form
 * @param array $form_state
 */
function mforms_example_ajax_form_submit($form, &$form_state) {
  mforms_submit('ajax_forms', $form, $form_state);
}
