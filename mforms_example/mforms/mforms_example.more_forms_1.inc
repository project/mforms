<?php

/**
 * @file
 * Provides example of multi forms at one page.
 */


/**
 * First form step.
 *
 * @param array $forms_state
 * @param array $next_step
 * @param mixed $params
 * @return array
 */
function _mforms_example_more_forms_1_s1(&$forms_state, &$next_step, $params) {
  $next_step = '_mforms_example_more_forms_1_s2';

  $controls = mforms_controls_get('more_forms_1');
  $controls->setControlsLabels(t('Edit form 1'));

  $form['field1'] = array(
    '#title' => t('Field 1'),
    '#markup' => isset($_SESSION['field1']) ? $_SESSION['field1'] : NULL,
  );

  return $form;
}

/**
 * We need to implement submit so that mforms will get us to next step.
 */
function _mforms_example_more_forms_1_s1_submit() {

}

/**
 * Second form step.
 *
 * @param array $forms_state
 * @param string $next_step
 * @param mixed $params
 * @return array
 */
function _mforms_example_more_forms_1_s2(&$forms_state, &$next_step, $params) {

  $controls = mforms_controls_get('more_forms_1');
  $controls->setControlsLabels(t('Submit'));
  $controls->setCancelPath('mforms/example/more_forms');

  $form['field1'] = array(
    '#type' => 'textfield',
    '#title' => t('Field 1'),
    '#required' => FALSE,
    '#default_value' => isset($_SESSION['field1']) ? $_SESSION['field1'] : NULL,
  );

  return $form;
}

/**
 * Submit callback of second form step.
 *
 * @param array $form
 * @param array $form_state
 */
function _mforms_example_more_forms_1_s2_submit($form, &$form_state) {
  $_SESSION['field1'] = $form_state['values']['field1'];
  drupal_set_message(t('Information has been updated'));
  mforms_clean('more_forms_1');
}
