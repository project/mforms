<?php

/**
 * @file
 *    Mforms ajax example.
 */

/**
 * Builds first form step with form element loaded by ajax call.
 *
 * @param array $form_state
 * @param string $next_step
 * @param mixed $params
 * @return array
 */
function _mforms_example_ajax_forms_s1(&$form_state, &$next_step, $params) {
  $next_step = '_mforms_example_ajax_forms_s2';

  $form = array();
  $values = mforms_get_vals('ajax_forms', '_mforms_example_ajax_forms_s1');

  $form['switch'] = array(
    '#type' => 'select',
    '#title' => 'Get me form element of type:',
    '#options' => array(
      'textfield' => t('Textfield'),
      'textarea' => t('Textarea'),
    ),
    '#default_value' => isset($values['switch']) ? $values['switch'] : NULL,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => '_mforms_example_ajax_forms_load_element',
      'wrapper' => 'new-element-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['new-element-wrapper'] = array(
    '#markup' => '',
    '#suffix' => '</div>',
    '#prefix' => '<div id="new-element-wrapper">'
  );

  if (isset($values['switch']) || isset($form_state['values']['switch'])) {
    $form['new-element-wrapper']['ajax_field'] = array(
      '#type' => isset($form_state['values']['switch']) ? $form_state['values']['switch'] : $values['switch'],
      '#title' => t('Element loaded by ajax'),
      '#default_value' => isset($values['ajax_field']) ? $values['ajax_field'] : NULL,
    );
  }

  $form['#after_build'][] = '_mforms_example_init_mforms';

  return $form;
}

/**
 * Loads built form element as a result of ajax call.
 *
 * @param array $form
 * @param array $form_state
 * @return array
 */
function _mforms_example_ajax_forms_load_element($form, &$form_state) {
  return $form['new-element-wrapper'];
}

/**
 * Need to implement submit to get to the next step.
 */
function _mforms_example_ajax_forms_s1_submit() {

}

/**
 * Second form step.
 *
 * @param array $form_state
 * @param string $next_step
 * @param array $params
 * @return array
 */
function _mforms_example_ajax_forms_s2(&$form_state, &$next_step, $params) {
  $values = mforms_get_vals('ajax_forms', '_mforms_example_ajax_forms_s1');

  $form = array();

  $form['info_choosed_element'] = array(
    '#title' => t('Choosed form elemement'),
    '#markup' => $values['switch'],
    '#type' => 'item',
  );

  $form['info_value'] = array(
    '#title' => t('Submitted value'),
    '#markup' => $values['ajax_field'],
    '#type' => 'item',
  );

  return $form;
}
