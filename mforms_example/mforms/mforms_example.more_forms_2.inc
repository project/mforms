<?php

/**
 * @file
 * Provides example of multi forms at one page.
 */


/**
 * First form step.
 *
 * @param array $forms_state
 * @param array $next_step
 * @param mixed $params
 * @return array
 */
function _mforms_example_more_forms_2_s1(&$form_state, &$next_step, $params) {
  $next_step = '_mforms_example_more_forms_2_s2';

  $controls = mforms_controls_get('more_forms_2');

  $controls->setControlsLabels(t('Edit form 2'));

  $form['field2'] = array(
    '#title' => t('Field 2'),
    '#markup' => isset($_SESSION['field2']) ? $_SESSION['field2'] : NULL,
  );

  return $form;
}

/**
 * We need to implement submit so that mforms will get us to next step.
 */
function _mforms_example_more_forms_2_s1_submit() {

}

/**
 * Second form step.
 *
 * @param array $forms_state
 * @param string $next_step
 * @param mixed $params
 * @return array
 */
function _mforms_example_more_forms_2_s2(&$forms_state, &$next_step, $params) {

  $controls = mforms_controls_get('more_forms_2');

  $controls->setControlsLabels(t('Submit'));
  $controls->setCancelPath('mforms/example/more_forms');

  $form['field2'] = array(
    '#type' => 'textfield',
    '#title' => t('Field 2'),
    '#required' => FALSE,
    '#default_value' => isset($_SESSION['field2']) ? $_SESSION['field2'] : NULL,
  );

  return $form;
}

/**
 * Submit callback of second form step.
 *
 * @param array $form
 * @param array $form_state
 */
function _mforms_example_more_forms_2_s2_submit($form, &$form_state) {
  $_SESSION['field2'] = $form_state['values']['field2'];
  drupal_set_message(t('Information has been updated'));
  mforms_clean('more_forms_2');
}
