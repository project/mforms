================================================================================
ABOUT Mforms
================================================================================
Mforms module is a set of tools that help to create advanced multistep forms in
Drupal. It can be modified and extended so a developer has full control of the
way how individual form steps will be handeled, how and where will be between
form steps data stored, which step will follow, how form controls will act and
how the output will be displayed.

The main differences from existing multistep solutions for Drupal are
robustness, flexibility and extensibility. You can even dynamically change the
steps sequence and jump from one step to another regardless of their order.

================================================================================
FEATURES
================================================================================
- Statemachine that handles steps flow - no need for "iffing" logic of which
step is to follow.
- Store that keeps data between form steps. Currently available session store
and $form_state store.
- Three different out-of-the-box form controls.
- You can easily extend existing controls implementations to alter its behavior.
- Full compatibility with with Drupal form API.
- Each form step carries info of which step is current and which is previous
and next.
- Theming is simple as mforms provide css classes with current status. So
creating a cool looking web wizard with progress indicator is just a matter of
use of mforms and knowledge of css.

================================================================================
INSTALLATION AND CONFIGURATION
================================================================================
Mforms is a library, so by itself it does not provide any functionality (besides
the real-life examples in the mforms example module). Therefore there is no
configuration or installation besides enabling the module in the module list.

================================================================================
BASIC USAGE
================================================================================
Included module mforms_example contains several real life examples that are
documented in code. To find out what it does visit /mforms page after you enable
mforms_example module.

Mforms architecture
--------------------------------------------------------------------------------
For faster dive into mforms here is the basic architecture:

The form steps
..............
Form steps itself are allways in a separate file in mforms subdirectory of your
module. The name of this file is as follows: MODULE_NAME.STORE_KEY.inc

Drupal page callback
....................
The entry point for a multistep form is a page callback. In this callback mforms
must get initiated. That means you need to instantiate desired store and
controls implementations and call mforms_init_module() function that besides
other things includes the form steps file described above. Note that the second
parameter of mforms_init_module() is $store_key. This must be the same as the
STORE_KEY used in the form steps file name.

Lastly the page callback should return built form by calling drupal_get_form().

Drupal form callbacks
.....................
The form lifecycle is handeled by Drupal. Therefore there must be regular form
callbacks: FORM_ID(), FORM_ID_validate() and FORM_ID_sbumit(). Inside these
mforms takes over as can be seen in the mforms_example.pages.inc line 72 to 107
and calls appropriate form steps or their _validate or _submit callbacks.

================================================================================
SPONSOR
================================================================================

This module is a product of blueMinds (http://blueminds.eu) web development
company.