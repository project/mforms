<?php

/**
 * @file
 * Single class that is an implementation of controls.
 * This implementation is suitable for a few step forms.
 */

/**
 * Simple implementation of form controls
 */
class MformsSimpleControls extends MformsControls {

  private static $instances = array();

  private $label_back;
  private $label_continue;
  private $label_submit;
  private $label_delete;

  private $weight_back = 11;
  private $weight_continue = 10;
  private $weight_submit = 10;
  private $weight_delete = 12;

  private $attributes_back = array();
  private $attributes_continue = array();
  private $attributes_submit = array();
  private $attributes_delete = array();


  /**
   * @static
   * @param MformsIstore $store
   * @param MformsSteps $steps
   * @param null $init
   * @return MformsControls
   */
  public static function getInstance(MformsIstore $store, MformsSteps $steps, $init = NULL) {
    if (empty(self::$instances[$store->getKey()])) {
      self::$instances[$store->getKey()] = new MformsSimpleControls($store, $steps);
    }

    return self::$instances[$store->getKey()];
  }

  private function __construct(MformsIstore $store, MformsSteps $steps) {
    $this->store = $store;
    $this->steps = $steps;

    // Init default labels
    $this->label_continue = t('Continue');
    $this->label_back = t('Back');
    $this->label_submit = t('Submit');
    $this->label_delete = t('Reset');

    $this->attributes_delete = array('onclick' => 'return confirm("' . t('This action will reset the form and all data you have entered will be lost. Do you whish to continue?') . '")');
  }

  function doSubmitOnClickedAction() {
    return FALSE;
  }

  /**
   * Gets form controls.
   * This method must be used to get form controls as they assure
   * proper working of the state machine.
   *
   * @return array
   *    Controls form definition.
   */
  function getControls() {
    $form = array();

    if ($this->isSingleStep()) {
      return $form;
    }

    $form['delete-all'] = array(
      '#type' => 'submit',
      '#value' => $this->label_delete,
      '#weight' => $this->weight_delete,
      '#attributes' => $this->attributes_delete,
      '#limit_validation_errors' => array(),
      '#submit' => array('_mforms_cancel_submit'),
      '#store_key' => $this->store->getKey(),
    );

    if ($this->steps->getPrev() != NULL) {
      $form['callback-' . $this->steps->getPrev()] = array(
        '#type' => 'submit',
        '#value' => $this->label_back,
        '#weight' => $this->weight_back,
        '#attributes' => $this->attributes_back,
        '#limit_validation_errors' => array(),
        '#submit' => array('_mforms_back_submit'),
        '#store_key' => $this->store->getKey(),
      );
    }

    if ($this->steps->getNext() != NULL) {
      $form['continue'] = array('#type' => 'submit', '#value' => $this->label_continue, '#weight' => $this->weight_continue, '#attributes' => $this->attributes_continue);
    }
    else {
      $form['submit'] = array('#type' => 'submit', '#value' => $this->label_submit, '#weight' => $this->weight_submit, '#attributes' => $this->attributes_submit);
    }

    return $form;
  }

  /**
   * Sets custom attributes for controls.
   *
   * @param array $back
   * @param array $continue
   * @param array $submit
   * @param array $delete
   */
  function setControlsAttributes($back, $continue, $submit, $delete) {
    if (is_array($back)) {
      $this->attributes_back = $back;
    }
    if (is_array($continue)) {
      $this->attributes_continue = $continue;
    }
    if (is_array($submit)) {
      $this->attributes_delete = $delete;
    }
  }

  /**
   * Sets custom labels for form controls.
   *
   * @param string $back
   * @param string $continue
   * @param string $submit
   * @param string $delete
   */
  function setControlsLabels($back, $continue, $submit, $delete) {
    if ($back != NULL && trim($back) != "") {
      $this->label_back = $back;
    }
    if ($continue != NULL && trim($continue) != "") {
      $this->label_continue = $continue;
    }
    if ($submit != NULL && trim($submit) != "") {
      $this->label_submit = $submit;
    }
    if ($delete != NULL && trim($delete) != "") {
      $this->label_delete = $delete;
    }
  }

  /**
   * Sets weights for form control elements.
   *
   * @param int $back
   * @param int $continue
   * @param int $submit
   * @param int $delete
   */
  function setControlsWeights($back, $continue, $submit, $delete) {
    $this->weight_back = $back;
    $this->weight_continue = $continue;
    $this->weight_submit = $submit;
    $this->weight_delete = $delete;
  }
}
