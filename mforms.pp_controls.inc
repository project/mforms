<?php

/**
 * @file
 * Single class that is a implementation of controls.
 * This implementation can be used for simple edit - submit/cancel workflow.
 */

/**
 * Controls - edit - submit, cancel
 */
class MformsPPControls extends MformsControls {

  private static $instances = array();

  private $label_submit;
  private $attributes_submit;

  private $callback = NULL;


  public static function getInstance(MformsIstore $store, MformsSteps $steps, $init = NULL) {
    if (empty(self::$instances[$store->getKey()])) {
      self::$instances[$store->getKey()] = new MformsPPControls($store, $steps);
    }

    return self::$instances[$store->getKey()];
  }

  private function __construct(MformsIstore $store, MformsSteps $steps) {
    $this->store = $store;
    $this->steps = $steps;

    $this->label_submit = t('Edit');
  }

  function isReset() {
    // Does not apply for workflow defined by this controls implementation.
    return FALSE;
  }

  /**
   * Gets form controls.
   * This method must be used to get form controls as they assure
   * proper working of the state machine.
   *
   * @return array
   *   Controls form definition.
   */
  function getControls() {
    $form = array();

    if ($this->isSingleStep()) {
      return $form;
    }

    if ($this->steps->getPrev() != NULL) {
      $form['cancel'] = array('#value' => l(t('Cancel'), $this->cancel_path), '#weight' => 101);
    }

    $form['submit'] = array('#type' => 'submit', '#value' => $this->label_submit, '#weight' => 100, '#attributes' => $this->attributes_submit);

    return $form;
  }

  /**
   * Sets custom labels for form controls.
   *
   * @param string $submit
   */
  function setControlsLabels($submit) {
    if ($submit != NULL && trim($submit) != "") {
      $this->label_submit = $submit;
    }
  }

  /**
   * Sets custom attributes for controls.
   *
   * @param array $submit
   */
  function setControlsAttributes($submit) {
    if (is_array($submit)) {
      $this->attributes_submit = $submit;
    }
  }
}
