<?php

/**
 * @file
 * Defines basics of controls class. If you want fully custom
 * form controls, you need to implement this interface, so that
 * the instance of your implementation can be used internaly by mforms.
 *
 * In case you want to only customize existing controls
 * override any of the existing implementations of this interface or
 * you may use the abstract implementation MformsControls.
 */

/**
 * This interface defines form controls used by mforms
 */
interface MformsIcontrols {

  static function getInstance(MformsIstore $store, MformsSteps $steps, $init = NULL);

  /**
   * Gets form controls as defined by Drupal form API
   *
   * @return array
   */
  function getControls();

  /**
   * Setter method to set clicked button.
   *
   * @param string $clicked_button_id
   */
  function setClickedButton($clicked_button_id);

  /**
   * Gets clicked action
   *
   * @return string - clicked action callback
   */
  function getClickedAction();

  /**
   * Gets additional param from clicked action
   *
   * @return string
   */
  function getClickedParam();

  /**
   * Determines if the form is singlestep
   *
   * @return boolean
   */
  function isSingleStep();

  /**
   * Determines if user inserted values are going to be reset
   *
   * @return boolean
   */
  function isReset();

  /**
   * Tels the statemachine if to call submit callback on user requested step
   *
   * @return boolean
   */
  function doSubmitOnClickedAction();

  /**
   * Sets path to which header is sent after cancel
   *
   * @param string $cancel_path
   */
  function setCancelPath($cancel_path);
}

/**
 * Provides generic functionality to simplify creation of final
 * implementations. If considering own controls implementation
 * this abstract class is a good startpoint.
 */
abstract class MformsControls implements MformsIcontrols {

  /**
   * Reference to store object
   *
   * @var MformsIstore
   */
  protected $store;

  /**
   * Reference to steps object
   *
   * @var MformsSteps
   */
  protected $steps;

  protected $single_step = FALSE;
  protected $cancel_path;

  /**
   * Sets single step - no form controls is displayed
   *
   */
  function setSingleStep() {
    $this->single_step = TRUE;
  }

  function isSingleStep() {
    return $this->single_step;
  }

  function setCancelPath($path) {
    $this->cancel_path = $path;
  }

  function setClickedButton($clicked_button) {
    $this->store->setStore('clicked_button', $clicked_button);
  }

  function getClickedAction() {
    $clicked_button = $this->store->getStore('clicked_button');

    if (empty($clicked_button)) {
      return NULL;
    }

    $clicked_button_id = $clicked_button['#id'];

    if (strpos($clicked_button_id, 'edit-callback') !== FALSE) {
      return str_replace(array('edit-callback', '-'), array('', '_'), $clicked_button_id);
    }

    return NULL;
  }

  function getClickedParam() {
    $clicked_button = $this->store->getStore('clicked_button');

    if (empty($clicked_button)) {
      return NULL;
    }

    $clicked_button_id = $clicked_button['#id'];

    if (strpos($clicked_button_id, 'edit-callback') !== FALSE) {
      return isset($clicked_button['#attributes']['mform_param']) ? $clicked_button['#attributes']['mform_param'] : NULL;
    }

    return NULL;
  }

  function doSubmitOnClickedAction() {
    return function_exists($this->getClickedAction());
  }

  function isReset() {
    $clicked_button = $this->store->getStore('clicked_button');
    return strpos($clicked_button['#id'], 'delete-all') !== FALSE;
  }
}
