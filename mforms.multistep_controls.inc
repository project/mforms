<?php

/**
 * @file
 * Single class that is an implementation of controls.
 * This implementation provides advanced controls for multistep form
 * where each step gets own button.
 */

/**
 * Provides functionality for forms with more than two steps where you
 * can navigate between individual steps.
 */
class MformsMultiStepControls extends MformsControls {

  private static $instances = array();

  private $steps_definition = array();

  private $label_back;
  private $label_continue;
  private $label_submit;
  private $label_delete;

  private $weight_back = 11;
  private $weight_continue = 10;
  private $weight_submit = 10;
  private $weight_delete = 12;

  private $attributes_back = array();
  private $attributes_continue = array();
  private $attributes_submit = array();
  private $attributes_delete = array();

  private $steps_label;


  public static function getInstance(MformsIstore $store, MformsSteps $steps, $steps_definition = NULL) {
    if (empty(self::$instances[$store->getKey()])) {
      self::$instances[$store->getKey()] = new MformsMultiStepControls($store, $steps, $steps_definition);
    }

    return self::$instances[$store->getKey()];
  }

  private function __construct(MformsIstore $store, MformsSteps $steps, $steps_definition) {
    $this->store = $store;
    $this->steps = $steps;
    $this->steps_definition = $steps_definition;

    // Init default labels
    $this->label_continue = t('Continue ››');
    $this->label_back = t('Back');
    $this->label_submit = t('Submit');
    $this->label_delete = t('Reset');
  }

  function getControls() {
    $form = array();

    if ($this->isSingleStep()) {
      return $form;
    }

    $form['steps_btns_wrapper'] = array(
      '#type' => 'fieldset',
      '#weight' => -1000,
      '#title' => $this->getStepsLabel()
    );

    $is_highlightable = TRUE;
    foreach ($this->steps_definition as $id => $definition) {
      $disabled = TRUE;
      if ($this->store->getStore($id) && !$this->isSingleStep()) {
        $disabled = FALSE;
      }

      $class = '';
      if ($this->steps->getCurr() == $id || $is_highlightable) {
        $class = 'higlighted-step';
      }

      if ($this->steps->getCurr() == $id) {
        $class .= ' current-step';
        $is_highlightable = FALSE;
      }
      elseif ($disabled) {
        $class .= ' disabled-step';
      }
      elseif (in_array($id, $this->steps->getSteps())) {
        $class .= ' active-step';
      }

      $form['steps_btns_wrapper']['callback_' . $id] = array(
        '#type' => 'submit',
        '#value' => $definition['value'],
        '#weight' => $definition['weight'],
        '#disabled' => $disabled,
        '#attributes' => array('class' => array($class)),
        '#limit_validation_errors' => array(),
        '#submit' => array('_mforms_back_submit'),
        '#store_key' => $this->store->getKey(),
      );
    }

    if ($this->steps->getNext() != NULL) {
      $form['continue'] = array(
        '#type' => 'submit',
        '#value' => $this->label_continue,
        '#weight' => $this->weight_continue,
        '#attributes' => $this->attributes_continue,
        '#limit_validation_errors' => array(),
        '#submit' => array('_mforms_back_submit'),
        '#store_key' => $this->store->getKey(),
      );
    }
    else {
      $form['submit'] = array('#type' => 'submit', '#value' => $this->label_submit,
        '#weight' => $this->weight_submit, '#attributes' => $this->attributes_submit);
    }

    if (count($this->attributes_delete) == 0) {
      $this->attributes_delete = array('onclick' =>
        'return confirm("' . t('This action will reset all values you have entered. Do you wish to continue?') . '");');
    }
    $form['delete-all'] = array(
      '#type' => 'submit',
      '#value' => $this->label_delete,
      '#weight' => $this->weight_delete,
      '#attributes' => $this->attributes_delete,
      '#limit_validation_errors' => array(),
      '#submit' => array('_mforms_cancel_submit'),
      '#store_key' => $this->store->getKey(),
    );

    return $form;
  }

  /**
   * Returns true if any of the steps buttons has been clicked.
   *
   * @return boolean
   */
  function doSubmitOnClickedAction() {
    $step_definition = $this->steps_definition[$this->steps->getCurr()];
    return isset($step_definition['submit']) ? $step_definition['submit'] : FALSE;
  }

  /**
   * Sets custom attributes for controls.
   *
   * @param array $back
   * @param array $continue
   * @param array $submit
   * @param array $delete
   */
  function setControlsAttributes($back, $continue, $submit, $delete) {
    if (is_array($back)) {
      $this->attributes_back = $back;
    }
    if (is_array($continue)) {
      $this->attributes_continue = $continue;
    }
    if (is_array($submit)) {
      $this->attributes_submit = $submit;
    }
    if (is_array($delete)) {
      $this->attributes_delete = $delete;
    }
  }

  /**
   * Sets custom labels for form controls.
   *
   * @param string $back
   * @param string $continue
   * @param string $submit
   * @param string $delete
   */
  function setControlsLabels($back, $continue, $submit, $delete) {
    if ($back != NULL && trim($back) != "") {
      $this->label_back = $back;
    }
    if ($continue != NULL && trim($continue) != "") {
      $this->label_continue = $continue;
    }
    if ($submit != NULL && trim($submit) != "") {
      $this->label_submit = $submit;
    }
    if ($delete != NULL && trim($delete) != "") {
      $this->label_delete = $delete;
    }
  }

  /**
   * Sets weights for form control elements.
   *
   * @param int $back
   * @param int $continue
   * @param int $submit
   * @param int $delete
   */
  function setControlsWeights($back, $continue, $submit, $delete) {
    $this->weight_back = $back;
    $this->weight_continue = $continue;
    $this->weight_submit = $submit;
    $this->weight_delete = $delete;
  }

  /**
   * Sets label displayed above steps buttons.
   *
   * @param string $steps_label
   */
  function setStepsLabel($steps_label) {
    $this->steps_label = $steps_label;
  }

  /**
   * Gets steps buttons label.
   *
   * @return string
   */
  function getStepsLabel() {
    return $this->steps_label;
  }
}
