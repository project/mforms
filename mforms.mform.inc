<?php

/**
 * @file
 * Single class - a utility object that provide statemachine functionality.
 */

/**
 * State machine for multistep Drupal forms and/or dealing with complex forking logic.
 *
 * It provides following features:
 *   - automatically handles stepping from one state to next and vise-versa
 *   - fully implements Drupal's form lifecycle - form render, data validate, data submit
 *   - provides information about current, previous and next step
 *   - provides form controls to navigate through available steps
 */
class Mform {

  private static $instances = array();

  /**
   * Reference to store object.
   *
   * @var MformsIstore
   */
  private $store = NULL;

  /**
   * Reference to steps object.
   *
   * @var MformsSteps
   */
  private $steps = NULL;

  /**
   * Reference to controls object.
   *
   * @var MformsIcontrols
   */
  private $controls = NULL;


  private function __construct() {

  }

  /**
   * Init the state machine object and get its singleton.
   *
   * @param MformsIstore $store
   *    Storage where to store data across steps.
   * @param MformsSteps $steps
   *    Steps object.
   * @param MformsIcontrols $controls
   *    Controls object.
   * @param string $first_step
   *    The first step of the state machine chain.
   * @return Mform
   *    Singleton object.
   */
  public static function getInstance(MformsIstore $store, MformsSteps $steps, MformsIcontrols $controls = NULL, $first_step = NULL) {

    // If singleton instance is NULL, instantiate the Mform.
    if (empty(self::$instances[$store->getKey()])) {
      self::$instances[$store->getKey()] = new Mform();
    }

    // If store is provided, reset it.
    if ($store != NULL) {
      self::$instances[$store->getKey()]->store = $store;
    }

    // If $steps is provided, reset it.
    if ($steps != NULL) {
      self::$instances[$store->getKey()]->steps = $steps;
    }

    // If controls is provided, reset it.
    if ($controls != NULL) {
      self::$instances[$store->getKey()]->controls = $controls;
    }

    // Make sure we have all must-be objects.
    if (self::$instances[$store->getKey()]->store == NULL) {
      throw new Exception('Store not provided');
    }
    if (self::$instances[$store->getKey()]->steps == NULL) {
      throw new Exception('Steps not provided');
    }

    // If we do not have current step, init steps from provided args.
    if (self::$instances[$store->getKey()]->steps->getCurr() == NULL && $first_step != NULL) {
      self::$instances[$store->getKey()]->steps->addStep($first_step);
      self::$instances[$store->getKey()]->steps->setCurr($first_step);
    }

    // Return instantiated and set mfrom object.
    return self::$instances[$store->getKey()];
  }

  /**
   * Main caller method.
   *
   * @param string $type
   *    Callback method type.
   *    - 'build' - expects to call functions that build form definition array
   *    - 'validate' - validate actions
   *    - 'submit' - submit action that should process the submitted data.
   *      It also shifts the internal pointer to the next step.
   * @param array $form
   *    Form definition array as defined by Drupal.
   * @param array $form_state
   *    Form state Drupal array.
   * @param array $params
   *    Array of additonal params/objects that should be passed in to
   *    the build callback.
   * @return mixed
   *    On build action it returns Drupal's form array on other void
   * @throws Exception
   *    If: controls are not loaded; invalid callback is provided;
   *    unable to load form from formstep.
   */
  public function call($type, $form, &$form_state, $params = array()) {

    if ($this->controls == NULL) {
      throw new Exception('Unable to load controls object');
    }

    if ($type == 'submit') {
      $form_state['rebuild'] = TRUE;
      // Set submitted data into store
      $this->setStore($form_state['values']);
    }

    if (!empty($form_state['clicked_button'])) {
      $this->controls->setClickedButton($form_state['clicked_button']);
    }

    // If reset action was called, clear store and exit any
    // other code execution.
    if ($this->controls->isReset()) {
      $this->store->clearStore();
    }

    // If we have clicked action - user requested other then the next step.
    $callback = $this->controls->getClickedAction();
    if (!empty($callback)) {

      if (!in_array($callback, $this->steps->getSteps())) {
        throw new Exception('Invalid callback ' . $callback);
      }

      // We want to run validate and submit callback for the next step
      // but the build callback should be run for clickedAction.
      if ($this->controls->doSubmitOnClickedAction()) {
        if ($type == 'submit') {
          $this->callSubmit($form, $form_state);
          return;
        }
        elseif ($type == 'build') {
          $this->steps->setCurr($callback);
          $ret = $this->callBuild($form_state, $params);
          // Unset the clicked button so that it is not used in following
          // regular lifecycle but not before build action is triggered so
          // that we have clicked_param available within the form build action.
          $this->controls->setClickedButton(NULL);
          return $ret;
        }
      }
      // Just run the build callback as defined by clickedAction.
      elseif (!$this->controls->doSubmitOnClickedAction()) {
        if ($type == 'build') {
          $this->steps->setCurr($callback);
          return $this->callBuild($form_state, $params);
        }
        else {
          return;
        }
      }
    }

    // Running regular lifecycle.
    if ($type == 'build') {
      return $this->callBuild($form_state, $params);
    }
    elseif ($type == 'validate' && !$this->controls->isSingleStep()) {
      $this->callValidate($form, $form_state);
    }
    elseif ($type == 'submit' && !$this->controls->isSingleStep()) {
      $this->callSubmit($form, $form_state);
    }
  }

  /**
   * Calling form render callbacks.
   *
   * @param array $form_state
   * @param array $params
   * @return array
   *    Should be form definition.
   * @throws Exception
   *    Throws an exception if form definition could not be retrieved.
   */
  private function callBuild($form_state, $params) {
    $next_step = NULL;

    // Call the callback function.
    $step = $this->steps->getCurr();
    if (!function_exists($step)) {
      return NULL;
    }

    $return_value = $step($form_state, $next_step, $params);

    if (!is_array($return_value)) {
      throw new Exception('Error retrieving form definition from step ' . $step);
    }

    // Add next step to the stack, however only if it is not allready added.
    if ($next_step != NULL && array_search($next_step, $this->steps->getSteps()) === FALSE) {
      $this->steps->addStep($next_step);
    }
    $return_value['curr_step'] = array(
      '#type' => 'value', '#value' => $step
    );
    $return_value['prev_step'] = array(
      '#type' => 'value', '#value' => $this->steps->getPrev()
    );
    $return_value['next_step'] = array(
      '#type' => 'value', '#value' => $this->steps->getNext()
    );
    $return_value['steps'] = array(
      '#type' => 'value', '#value' => serialize($this->steps->getSteps())
    );

    return $return_value;
  }

  /**
   * Calling validate callbacks.
   *
   * @param array $form
   * @param array $form_state
   */
  private function callValidate($form, &$form_state) {
    $step = $this->steps->getCurr() . '_validate';

    if (!function_exists($step)) {
      return;
    }

    $step($form, $form_state);
  }

  /**
   * Calling submit callbacks.
   *
   * @param array $form
   * @param array $form_state
   */
  private function callSubmit($form, &$form_state) {
    $step = $this->steps->getCurr() . '_submit';

    if (!function_exists($step)) {
      return;
    }

    $step($form, $form_state);

    // If we have next step - set it as current.
    if ($this->steps->getNext() != NULL) {
      $this->steps->setCurr($this->steps->getNext());
    }
  }

  /**
   * Set store used to store data between steps.
   *
   * @param mixed $data
   */
  private function setStore($data) {
    if ($this->steps->getCurr()) {
      $this->store->setStore($this->steps->getCurr(), $data);
    }
  }

  /**
   * Gets data from store.
   *
   * @param string $step
   * @return mixed
   */
  function getStore($step = NULL) {
    if ($step == NULL) {
      return $this->store->getStore($this->steps->getCurr());
    }
    return $this->store->getStore($step);
  }
}
